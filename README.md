# Contacts

The contacts app is as close as possible to the contacts in the iPhone.



What is implemented in stages:

-Add/edit contact page: basic implementation
-Add/edit contact page: add a photo to contact
-Add/edit contact page: handle no access to camera / gallery
-Add/edit contact page: delete contact
-Create contacts list page without records
-Create contacts list page with records
-Contact detail page: edit contact
-Contact detail page: basic implementation
-Contact detail page: delete contact
-Add/edit contact page: validate First & Last Name

-Add/edit contact page: edit photo via tap in contacts edit page
-Contacts list page: edit contacts mode
-Add/edit contact page: Phone number restrictions
-Contacts list page: edit/delete contact via swipe
-Add/edit contact page: validate Email
-Add/edit contact page: save contact permanently

-Contacts list page: filter contacts
-Setup application localization
-Contacts list page: split contacts by ABC sections

-Add/edit contact and detail page: add Birthday field
-Add/edit contact and detail page: add Height field
-Add/edit contact and detail page: add Notes field
-Add/edit contact and detail page: add Driver License field
-Refactor to use KVO

-Settings page: basic implementation
-Settings: first and last name order preference
-Settings: short / detailed view preference

![Alt Text](https://media.giphy.com/media/DyQQQ37Vw0mNVmoFJv/giphy.gif)
