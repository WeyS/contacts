//
//  RegViewCell.swift
//  contacts
//
//  Created by Maxim on 20.10.2021.
//

import UIKit
import AudioToolbox

class RegViewCell: UITableViewCell, UITextFieldDelegate {
    
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var validate: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var switchLabel: UISwitch!
    @IBOutlet weak var notes: UITextView!
    
    let datePicker = UIDatePicker()
    let heightPicker = UIPickerView()
    let toolBar = UIToolbar()
    let arr = [["1","2"],
               ["1","2","3","4","5","6","7","8","9","0"],
               ["1","2","3","4","5","6","7","8","9","0"]]
    
    func setContact(contact : CreateCell){
        label.text = contact.title
        textField.placeholder = contact.placeHolder
        validate.text = contact.validate
        if contact.title == "Birthday"{
            if #available(iOS 13.4, *) {
                setDatePicker()
            } else {
                
            }
        }
        if contact.title == "Height, cm"{
            setHeightPicker()
        }
    }
    
    func setDriver(licence: CreateCell){
        if licence.placeHolder == ""{
            label.text = licence.title
            textField.placeholder = licence.placeHolder
            textField.isEnabled = false
            switchLabel.isHidden = false
            switchLabel.addTarget(self, action: #selector(changedSwitch), for: .valueChanged)
        }
        if licence.placeHolder == "Driver's licence number"{
            label.text = licence.title
            textField.placeholder = licence.placeHolder
            textField.text?.removeAll()
            if switchLabel.isOn{
                isHidden = false
            }else{
                isHidden = true
            }
        }
    }
    
    func setNotes(notesCell: CreateCell){
        label.text = notesCell.title
        notes.isHidden = false
        textField.isHidden = true
        
    }
    
    @objc func changedSwitch(_ sender: UISwitch){
        guard let table = self.superview as? UITableView else { return }
        let row = table.cellForRow(at: IndexPath(row: 1, section: 1))
        if sender.isOn{
            row?.isHidden = false
        }else{
            row?.isHidden = true
            table.reloadRows(at: [IndexPath(row: 1, section: 1)], with: .automatic)
        }
    }
    
    @available(iOS 13.4, *)
    func setDatePicker(){
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        textField.inputView = datePicker
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButton))
        toolBar.setItems([doneButton], animated: true)
        textField.inputAccessoryView = toolBar
        datePicker.addTarget(self, action: #selector(dateChange), for: .valueChanged)
    }
    
    @objc func dateChange(){
        let forrmater = DateFormatter()
        forrmater.dateFormat = "dd/MM/yyyy"
        textField.text = forrmater.string(from: datePicker.date)
    }
    
    @objc func doneButton(){
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
        heightPicker.removeFromSuperview()
    }
    
    func setHeightPicker(){
        heightPicker.delegate = self
        heightPicker.dataSource = self
        textField.inputView = heightPicker
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButton))
        toolBar.setItems([doneButton], animated: true)
        textField.inputAccessoryView = toolBar
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        notes.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        label.textColor = .blue
        view.isHidden = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        label.textColor = .black
        view.isHidden = true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension RegViewCell: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        arr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        arr[component].count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        arr[component][row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var resultString = ""
        for index in 0..<arr.count{
            let digit = arr[index][pickerView.selectedRow(inComponent: index)]
            resultString += digit
        }
        textField.text = "\(resultString) cm"
    }
    
}

extension RegViewCell: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Notes"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count >= 120{
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate), nil)
            textView.textColor = .red
            textView.text.popLast()
        }else{
            textView.textColor = .black
        }
    }
}
