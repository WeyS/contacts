//
//  TableRegCell.swift
//  contacts
//
//  Created by Maxim on 24.10.2021.
//

import UIKit

class TableRegCell: UITableViewCell {
    
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    func setContact(contact : Contact, letter: String? = nil){
        personImage.image = UIImage(data: contact.photo)
        
        let rangeFirst = NSString(string: contact.firstName).range(of: letter ?? "")
        let rangeLast = NSString(string: contact.lastName).range(of: letter ?? "")
        let rangePhone = NSString(string: contact.phone).range(of: letter ?? "")
        let fistNameString = NSMutableAttributedString(string: ("\(contact.firstName) "))
        let lastNameString = NSMutableAttributedString(string: contact.lastName)
        let phoneString = NSMutableAttributedString(string: contact.phone)
        lastNameString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: rangeLast)
        fistNameString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: rangeFirst)
        phoneString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: rangePhone)
        fistNameString.append(lastNameString)
        firstName.attributedText = fistNameString
//        lastName.text = contact.lastName
        phone.attributedText = phoneString
        personImage.layer.cornerRadius = personImage.frame.size.width / 2
    }
    
    func shortContact(contact: Contact){
        phone.text = ""
    }
    
    func reverseName(contact: Contact){
        let first = contact.firstName
        let last = contact.lastName
        let fullNameArr = last + " " + first
        firstName.text = fullNameArr
    }
}
