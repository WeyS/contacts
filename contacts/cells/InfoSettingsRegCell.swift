//
//  CreateInfoSettingsCell.swift
//  contacts
//
//  Created by Maxim on 23.01.2022.
//

import UIKit

class InfoSettingsRegCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var decriptionLabel: UILabel!
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        self.accessoryType = selected ? .checkmark : .none
//    }
    
    func setInfoSettings(settings: CreateSettingsCell){
        mainLabel.text = settings.mainLabel
        decriptionLabel.text = settings.decriptionLabel
    }
    
    func setDisplaySettings(settings: CreateSettingsCell){
        mainLabel.text = settings.mainLabel
    }

}
