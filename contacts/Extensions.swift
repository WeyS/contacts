//
//  Extensions.swift
//  contacts
//
//  Created by Maxim on 13.01.2022.
//

import Foundation


extension String{
    func localizable() -> String{
        return NSLocalizedString(self, tableName: "Localizable", bundle: .main, value: self, comment: self)
    }
}
