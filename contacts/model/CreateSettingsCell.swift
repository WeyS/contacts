//
//  CreateSettingsCell.swift
//  contacts
//
//  Created by Maxim on 21.01.2022.
//

import Foundation

struct CreateSettingsCell{
    let mainLabel: String
    var decriptionLabel: String? = nil
}
