//
//  NewContacts.swift
//  contacts
//
//  Created by Maxim on 20.10.2021.
//

import Foundation

struct CreateCell {
    var title : String
    var placeHolder : String
    var validate : String? = nil
}
