//
//  Contact.swift
//  contacts
//
//  Created by Maxim on 21.10.2021.
//

import Foundation
import UIKit
import RealmSwift

//public protocol ImageCodable: Codable {}
//extension UIImage: ImageCodable {}
//
//extension ImageCodable where Self: UIImage {
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        self.init(data: try container.decode(Data.self))!
//    }
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encode(self.pngData()!)
//    }
//}

//struct Contact : Codable {
//    var firstName: String
//    var lastName: String
//    var phone: String
//    var email: String
//    var photo: UIImage
//}

class Contact: Object{
    @Persisted var firstName: String
    @Persisted var lastName: String
    @Persisted var phone: String
    @Persisted var email: String
    @Persisted var photo: Data
    @Persisted var date: String
    @Persisted var height: String
    @Persisted var driveNumber: String
    @Persisted var notes: String
    
    convenience init(firstName: String, lastName: String, phone: String, email: String, photo: Data, date: String, height: String, driveNumber: String, notes: String){
        self.init()
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.email = email
        self.photo = photo
        self.date = date
        self.height = height
        self.driveNumber = driveNumber
        self.notes = notes
    }
}
