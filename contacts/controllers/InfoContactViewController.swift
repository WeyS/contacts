//
//  InfoContactViewController.swift
//  contacts
//
//  Created by Maxim on 25.10.2021.
//

import UIKit
import RealmSwift


@available(iOS 13.4, *)
class InfoContactViewController: UIViewController {
    
    var dataCellContact: Contact!
    var index: Int?
    
    var realm = try! Realm()

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var birthDay: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var driveNumber: UILabel!
    @IBOutlet weak var notes: UILabel!
    var completionInfoToContact : ((Contact) -> ())?
    var completionDeleteToContact : ((Contact) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit".localizable(), style: .plain, target: self, action: #selector(editButton))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localizable(), style: .plain, target: self, action: #selector(canecelButton))
        image.image = UIImage(data: dataCellContact.photo)
        firstName.text = dataCellContact.firstName + " " + dataCellContact.lastName
        phoneNumber.text = dataCellContact.phone
        email.text = dataCellContact.email
        birthDay.text = dataCellContact.date
        height.text = dataCellContact.height
        driveNumber.text = dataCellContact.driveNumber
        notes.text = dataCellContact.notes
        image.layer.cornerRadius = image.frame.size.width / 2
    }
    
    @objc func editButton(){
        let vc = (storyboard?.instantiateViewController(identifier: "edit"))! as! EditViewController
        vc.oldContact = dataCellContact
        vc.completionToInfo = { [weak self] contact in
            guard let self = self else { return }
            self.firstName.text = contact.firstName + " " + contact.lastName
            self.phoneNumber.text = contact.phone
            self.image.image = UIImage(data: contact.photo)
            self.email.text = contact.email
            self.birthDay.text = contact.date
            self.height.text = contact.height
            self.dataCellContact.realm?.beginWrite()
            self.dataCellContact.firstName = contact.firstName
            self.dataCellContact.lastName = contact.lastName
            self.dataCellContact.phone = contact.phone
            self.dataCellContact.photo = contact.photo
            self.dataCellContact.email = contact.email
            self.dataCellContact.date = contact.date
            self.dataCellContact.height = contact.height
            try! self.dataCellContact.realm?.commitWrite()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func canecelButton(){
//        completionInfoToContact?(dataCellContact)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func deleteContact(_ sender: Any) {
        let alert : UIAlertController = UIAlertController(title: "Do you want to delete contact?".localizable(), message: nil, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes".localizable(), style: .default){
            UIAlertAction in
            self.completionDeleteToContact?(self.dataCellContact)
            self.navigationController?.popViewController(animated: true)
        }
        let noAction = UIAlertAction(title: "No".localizable(), style: .cancel){
                UIAlertAction in
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
}
