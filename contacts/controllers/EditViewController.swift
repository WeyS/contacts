//
//  EditViewController.swift
//  contacts
//
//  Created by Maxim on 12.11.2021.
//

import UIKit
import RealmSwift

@available(iOS 13.4, *)
class EditViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    var realm = try! Realm()
    
    var oldContact: Contact!
    var picker = UIImagePickerController()
    @IBOutlet weak var oldImage: UIImageView!
    @IBOutlet weak var first: UITextField!
    @IBOutlet weak var last: UITextField!
    @IBOutlet weak var phon: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var birthDay: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var licence: UITextField!
    @IBOutlet weak var notes: UITextView!
    
    let datePicker = UIDatePicker()
    let heightPicker = UIPickerView()
    let toolBar = UIToolbar()
    
    let arr = [["1","2","3","4","5","6","7","8","9","0"],
               ["1","2","3","4","5","6","7","8","9","0"],
               ["1","2","3","4","5","6","7","8","9","0"]]
    
    var completionToInfo : ((Contact) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save".localizable(), style: .plain, target: self, action: #selector(saveButton))
        oldImage.image = UIImage(data: oldContact.photo)
        oldImage.layer.cornerRadius = oldImage.frame.size.width / 2
        if first.text != nil {
            first.text = oldContact.firstName
        }
        if last.text != nil {
            last.text = oldContact.lastName
        }
        if phon.text != nil {
            phon.text = oldContact.phone
        }
        if email.text != nil {
            email.text = oldContact.email
        }
        birthDay.text = oldContact.date
        height.text = oldContact.height
        setDatePicker()
        setHeightPicker()
        licence.text = oldContact.driveNumber
        notes.text = oldContact.notes
    }
    
    @objc func saveButton(){
        self.oldContact.realm?.beginWrite()
        oldContact.photo = (oldImage.image!.jpegData(compressionQuality: 0.9)!)
        oldContact.firstName = first.text!
        oldContact.lastName = last.text!
        oldContact.phone = phon.text!
        oldContact.email = email.text!
        oldContact.date = birthDay.text!
        oldContact.height = height.text!
        oldContact.driveNumber = licence.text!
        oldContact.notes = notes.text!
        try! self.oldContact.realm?.commitWrite()
        completionToInfo?(oldContact)
//        navigationController?.popViewController(animated: true)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func setDatePicker(){
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        birthDay.inputView = datePicker
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButton))
        toolBar.setItems([doneButton], animated: true)
        birthDay.inputAccessoryView = toolBar
        datePicker.addTarget(self, action: #selector(dateChange), for: .valueChanged)
    }
    
    @objc func dateChange(){
        let forrmater = DateFormatter()
        forrmater.dateFormat = "dd/MM/yyyy"
        birthDay.text = forrmater.string(from: datePicker.date)
    }
    
    @objc func doneButton(){
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
        heightPicker.removeFromSuperview()
    }
    
    func setHeightPicker(){
        heightPicker.delegate = self
        heightPicker.dataSource = self
        height.inputView = heightPicker
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButton))
        toolBar.setItems([doneButton], animated: true)
        height.inputAccessoryView = toolBar
        
    }

    
    func areEqualImages(img1: UIImage, img2: UIImage) -> Bool {

        guard let data1 = img1.pngData() else { return false }
        guard let data2 = img2.pngData() else { return false }

        return data1 == data2
    }
    
    @IBAction func changePhoto(_ sender: Any) {
        if areEqualImages(img1: oldImage.image!, img2: UIImage(named: "Vector-1")!){
            let alert:UIAlertController=UIAlertController(title: "Choose variant", message: nil, preferredStyle: .actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: .default)
                {
                    UIAlertAction in
                    self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallary", style: .default)
                {
                    UIAlertAction in
                    self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                {
                    UIAlertAction in
                }

            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert:UIAlertController=UIAlertController(title: "What do you want to do", message: nil, preferredStyle: .actionSheet)
            let cameraAction = UIAlertAction(title: "Change photo", style: .default)
            {_ in
                let alert:UIAlertController=UIAlertController(title: "Choose variant", message: nil, preferredStyle: .actionSheet)
                let cameraAction = UIAlertAction(title: "Camera", style: .default)
                    {
                        UIAlertAction in
                        self.openCamera()
                }
                let gallaryAction = UIAlertAction(title: "Gallery", style: .default)
                    {
                        UIAlertAction in
                        self.openGallary()
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                    {
                        UIAlertAction in
                    }

                self.picker.delegate = self
                alert.addAction(cameraAction)
                alert.addAction(gallaryAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            let gallaryAction = UIAlertAction(title: "Remove photo", style: .default)
            {_ in 
                self.oldImage.image = UIImage(named: "Vector-1")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                {
                    UIAlertAction in
                }

            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.sourceType = UIImagePickerController.SourceType.camera
            self.present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
        }
    }
    func openGallary(){
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    //MARK:UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            self.oldImage.image = pickedImage
            oldImage.layer.cornerRadius = oldImage.frame.size.width / 2
            oldImage.clipsToBounds = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        print("picker cancel.")
    }
    
    
}

@available(iOS 13.4, *)
extension EditViewController: UIPickerViewDelegate , UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        arr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        arr[component].count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        arr[component][row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var resultString = ""
        for index in 0..<arr.count{
            let digit = arr[index][pickerView.selectedRow(inComponent: index)]
            resultString += digit
        }
        height.text = "\(resultString) cm"
    }
}

