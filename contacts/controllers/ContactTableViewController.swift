//
//  ContactTableViewController.swift
//  contacts
//
//  Created by Maxim on 23.10.2021.
//

import UIKit
import RealmSwift

@available(iOS 13.4, *)
class ContactTableViewController: UIViewController {
    
    var realm = try! Realm()
    var notificationToken: NotificationToken? = nil
    
    var contactDictionary = [String: [Contact]]()
    var keys = [String]()
    var alphabets = (97...122).map { "\(Character(UnicodeScalar.init($0)!))" }.map { $0.uppercased() }
    
    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var startNavBar: UINavigationBar!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addContact: UIButton!
//    var dataContacts:[Contact] = []
    var filtredContacts: [Contact] = []
    var dataContacts: Results<Contact>!
    var indexRow:Int!
    var indexSection:Int!
    
    var cheackDisplay: [Bool] = [false,true]
    var cheackInfo: [Bool] = [true,false]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataContacts = realm.objects(Contact.self)
        
        tableView.delegate = self
        tableView.dataSource = self

        let plus = UIBarButtonItem(image : UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(plusButton))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "gearshape"), style: .plain, target: self, action: #selector(settingsButton))
        let edit = UIBarButtonItem(title: "Edit".localizable(), style: .plain, target: self, action: #selector(toggleEditing))
        navigationItem.rightBarButtonItems = [edit,plus]
        let startAddButton: UIButton = addContact
        startAddButton.addTarget(self, action: #selector(addStartContact), for: .touchUpInside)
        
        searchController.obscuresBackgroundDuringPresentation = false
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search Contact".localizable()
        searchController.searchBar.searchTextField.autocapitalizationType = .none
        
        setContacts()

//        if let myData = UserDefaults.standard.value(forKey: "SavedContacts") as? Data{
//            dataContacts = try! PropertyListDecoder().decode(Array<Contact>.self, from: myData)
//            tableView.reloadData()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        notificationToken = dataContacts.observe { [weak self] (changes) in
            switch changes {
            case .initial:
                self?.tableView.reloadData()
            case .update:
                guard let self = self else { return }
                if self.indexSection != nil{
                    if self.contactDictionary[self.keys[self.indexSection]]?.count ?? 0 < 2{
                        self.contactDictionary[self.keys[self.indexSection]] = nil
                    }
                }
                if self.isFiltering{
                    self.contactDictionary.removeAll()
                }
                self.setContacts()
                self.tableView.reloadData()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        tableView.reloadData()
    }
    
    @objc func addStartContact(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(identifier: "doubleContactVC") as! SecondNewContactViewController
        newViewController.completionNewContact = { [weak self] contact in
            guard let self = self else { return }
//            self.dataContacts += contact
            self.tableView.reloadData()
        }
        self.present(newViewController, animated: true, completion: nil)
    }
    
    @objc func toggleEditing() {
        tableView.setEditing(!tableView.isEditing, animated: true)
        navigationItem.rightBarButtonItem?.title = tableView.isEditing ? "Done".localizable() : "Edit".localizable()
    }
    
    @objc func plusButton(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(identifier: "doubleContactVC") as! SecondNewContactViewController
        newViewController.completionNewContact = { [weak self] contact in
            guard let self = self else { return }
//            self.dataContacts += contact
            self.tableView.reloadData()
        }
        self.present(newViewController, animated: true, completion: nil)
    }

    @objc func settingsButton(){
        let settingsViewController = storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        settingsViewController.checkedDisplay = self.cheackDisplay
        settingsViewController.completitionDisplay = { [weak self] settings in
            guard let self = self else { return }
            self.cheackDisplay = settings
        }
        settingsViewController.checkedInfo = self.cheackInfo
        settingsViewController.completitionInfo = { [weak self] settings in
            guard let self = self else { return }
            self.cheackInfo = settings
        }
        
        self.navigationController?.pushViewController(settingsViewController, animated: true)
    }
    
    private func setContacts() {
        
        var temp = [String: [Contact]]()
        for contact in self.dataContacts{
            let firstName = contact.firstName
            let lastName = contact.lastName
            let email = contact.email
            if !firstName.isEmpty{
                let firstChar = "\(firstName.first!)".uppercased()
                if alphabets.contains(firstChar){
                    var array = temp[firstChar] ?? []
                    array.append(contact)
                    temp[firstChar] = array
                }
            }else if !lastName.isEmpty{
                let firstChar = "\(lastName.first!)".uppercased()
                if alphabets.contains(firstChar){
                    var array = temp[firstChar] ?? []
                    array.append(contact)
                    temp[firstChar] = array
                }
            }else if !email.isEmpty{
                let firstChar = "\(email.first!)".uppercased()
                if alphabets.contains(firstChar){
                    var array = temp[firstChar] ?? []
                    array.append(contact)
                    temp[firstChar] = array
                }
            }else{
                var array = temp["#"] ?? []
                array.append(contact)
                temp["#"] = array
            }
        }
        self.keys = Array(temp.keys).sorted()
        for key in self.keys { self.contactDictionary[key] = temp[key] }
    }
    
    func cellSelected(contact : Contact){
        let vc = (storyboard?.instantiateViewController(identifier: "InfoVC"))! as! InfoContactViewController
        vc.dataCellContact = contact
        vc.index = self.indexRow
        vc.completionInfoToContact = { [weak self] contact in
            guard let self = self else { return }
            
            self.indexSection = nil
            self.indexRow = nil
            
            self.tableView.reloadData()
        }
        vc.completionDeleteToContact = { [weak self] contact in
            guard let self = self else { return }
            
            
            if self.isFiltering{
                try! self.realm.write{
                    self.realm.delete(self.filtredContacts[self.indexRow])
                }
                self.filtredContacts.remove(at: self.indexRow)
                self.tableView.deleteRows(at: [IndexPath(row: self.indexRow, section: 0)], with: .automatic)
            }else{
                if self.contactDictionary[self.keys[self.indexSection]]!.count < 2{
                    self.contactDictionary[self.keys[self.indexSection]] = nil
                }else{
                    self.contactDictionary[self.keys[self.indexSection]]?.remove(at: self.indexRow)
                }
                
                try! self.realm.write{
                    self.realm.delete(contact)
                }
            }
            
            self.indexSection = nil
            self.indexRow = nil
            
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
}

@available(iOS 13.4, *)
extension ContactTableViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if contactDictionary.count == 0 {
            self.tableView.setEmpty(startView)
            navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.tableView.restore(startView)
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        if isFiltering{
            return 1
        }else{
            return contactDictionary.count
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering{
            return filtredContacts.count
        }else{
            return contactDictionary[keys[section]]?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering{
            return nil
        }else{
            if self.tableView(tableView, numberOfRowsInSection: section) > 0{
                return self.keys[section]
                }else{
                    return nil
                }
            }
        }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isFiltering{
            return nil
        }else{
            return keys
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "2", for: indexPath) as! TableRegCell
        
        if isFiltering{
            let filterRow = self.filtredContacts[indexPath.row]
            cell.setContact(contact: filterRow, letter: self.searchController.searchBar.text)
        }else{
            let row = self.contactDictionary[self.keys[indexPath.section]]![indexPath.row]
            if cheackDisplay[0] == true && cheackInfo[1] == true{
                cell.shortContact(contact: row)
                cell.reverseName(contact: row)
            }else if cheackDisplay[0] == true && cheackInfo[0] == true{
                cell.setContact(contact: row)
                cell.reverseName(contact: row)
            }else if cheackDisplay[1] == true && cheackInfo[0] == true{
                cell.setContact(contact: row)
            }else if cheackDisplay[1] == true && cheackInfo[1] == true{
                cell.setContact(contact: row)
                cell.shortContact(contact: row)
            }
        }
        
        
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(dataContacts), forKey: "SavedContacts")
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFiltering{
            let filterRow = filtredContacts[indexPath.row]
            cellSelected(contact: filterRow)
            indexRow = indexPath.row
        }else{
            let row = self.contactDictionary[self.keys[indexPath.section]]![indexPath.row]
            cellSelected(contact: row)
            indexRow = indexPath.row
            indexSection = indexPath.section
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if isFiltering{
            return nil
        }else{
            let delete = UITableViewRowAction(style: .destructive, title: "delete".localizable()){ (action , indexPath) in
            
            try! self.realm.write{
                self.realm.delete((self.contactDictionary[self.keys[indexPath.section]]?.remove(at:indexPath.row))!)
            }
            if self.contactDictionary[self.keys[indexPath.section]]!.count < 2{
                self.contactDictionary[self.keys[indexPath.section]] = nil
            }else if (self.contactDictionary[self.keys[indexPath.section]]!.last != nil){
                self.contactDictionary[self.keys[indexPath.section]]?.popLast()
            }else{
                self.contactDictionary[self.keys[indexPath.section]]?.remove(at: indexPath.row)
            }
                
            self.indexRow = nil
            self.indexSection = nil
            self.tableView.reloadData()
        }
            let edit = UITableViewRowAction(style: .normal, title: "edit".localizable()){ (action , indexPath) in
            let vc = self.storyboard?.instantiateViewController(identifier: "editInTable") as! EditInTableViewController

            let row = self.contactDictionary[self.keys[indexPath.section]]![indexPath.row]
            vc.contactInTable = row
            vc.completionEditTable = { [weak self] contact in
                
                if self!.contactDictionary[self!.keys[indexPath.section]]!.count < 2{
                    self!.contactDictionary[self!.keys[indexPath.section]]![indexPath.row] = contact
                    self!.contactDictionary[self!.keys[indexPath.section]] = nil
                }else{
                    self!.contactDictionary[self!.keys[indexPath.section]]![indexPath.row] = contact
                }
                
                self?.tableView.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        edit.backgroundColor = UIColor.green
        
        return [delete , edit]
    }
    }
}

extension UITableView {

    func setEmpty(_ view : UIView) {
        view.isHidden = false
    }

    func restore(_ view : UIView) {
        view.isHidden = true
    }
}

@available(iOS 13.4, *)
extension ContactTableViewController: UISearchResultsUpdating {
   func updateSearchResults(for searchController: UISearchController) {
       filterContentForSearchText(searchController.searchBar.text!)
   }
    private func filterContentForSearchText(_ searchText: String) {
        
        filtredContacts = dataContacts.filter({ (contact: Contact) -> Bool in
            
            return contact.firstName.lowercased().contains(searchText.lowercased()) ||                          contact.lastName.lowercased().contains(searchText.lowercased())  ||
                   contact.phone.lowercased().contains(searchText.lowercased())     ||
                   contact.email.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

