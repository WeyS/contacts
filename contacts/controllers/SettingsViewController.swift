//
//  SettingsViewController.swift
//  contacts
//
//  Created by Maxim on 21.01.2022.
//

import UIKit

class SettingsViewController: UIViewController {
    
    var settingsDisplay: [CreateSettingsCell] = []
    var settingsInfo: [CreateSettingsCell] = []
    var lastSelected: IndexPath?
    var lastSelected1: IndexPath?
    
    var checkedDisplay = [false,false]
    var checkedInfo = [false,false]
    
    var completitionDisplay: (([Bool]) -> ())?
    var completitionInfo: (([Bool]) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Settings"
        self.navigationItem.largeTitleDisplayMode = .never
        settingsDisplay = createDisplayCell()
        settingsInfo = createInfoCell()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(backButton))
    }
    func createDisplayCell() -> [CreateSettingsCell]{
        var tempArr: [CreateSettingsCell] = []
        
        let displayFirstSettings = CreateSettingsCell(mainLabel: "Last, First")
        let displaySecondSettings = CreateSettingsCell(mainLabel: "First, Last")
        tempArr.append(displayFirstSettings)
        tempArr.append(displaySecondSettings)
        return tempArr
    }
    func createInfoCell() -> [CreateSettingsCell]{
        var tempArr: [CreateSettingsCell] = []
        
        let infoFirstSettings = CreateSettingsCell(mainLabel: "Detailed", decriptionLabel: "Name + Phone")
        let infoSecondSettings = CreateSettingsCell(mainLabel: "Short", decriptionLabel: "Name only")
        tempArr.append(infoFirstSettings)
        tempArr.append(infoSecondSettings)
        return tempArr
    }
    
    @objc private func backButton(){
        completitionDisplay?(checkedDisplay)
        completitionInfo?(checkedInfo)
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return "Display Order"
        case 1:
            return "Info View"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        return tableView.backgroundColor = #colorLiteral(red: 0.9215685725, green: 0.9215685725, blue: 0.9215685725, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettginsCell", for: indexPath) as! InfoSettingsRegCell
        
        switch indexPath.section{
        case 0:
            let displaySettings = settingsDisplay[indexPath.row]
            cell.setDisplaySettings(settings: displaySettings)
            if checkedDisplay[1] == true && cell.mainLabel.text == "First, Last"{
                cell.accessoryType = .checkmark
            }else if checkedDisplay[0] == true && cell.mainLabel.text == "Last, First"{
                cell.accessoryType = .checkmark
            }
        case 1:
            let infoSettings = settingsInfo[indexPath.row]
            cell.setInfoSettings(settings: infoSettings)
            if checkedInfo[0] == true && cell.mainLabel.text == "Detailed"{
                cell.accessoryType = .checkmark
            }else if checkedInfo[1] == true && cell.mainLabel.text == "Short"{
                cell.accessoryType = .checkmark
            }
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let selected = self.lastSelected {
                tableView.cellForRow(at: selected)?.accessoryType = .none
                checkedDisplay[selected.row] = false
        }
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            checkedDisplay[indexPath.row] = true
            tableView.deselectRow(at: indexPath, animated: true)
            self.lastSelected = indexPath
        } else if indexPath.section == 1 {
            if let selected = self.lastSelected1 {
                tableView.cellForRow(at: selected)?.accessoryType = .none
                checkedInfo[selected.row] = false
        }
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            checkedInfo[indexPath.row] = true
            tableView.deselectRow(at: indexPath, animated: true)
            self.lastSelected1 = indexPath
        }
        print(checkedDisplay)
        print(checkedInfo)
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {

//        if let selectedIndexPathsInSection = tableView.indexPathsForSelectedRows?.filter({ $0.section == indexPath.section }), !selectedIndexPathsInSection.isEmpty {
//            selectedIndexPathsInSection.forEach({ tableView.deselectRow(at: $0, animated: false) })
//        }
//        tableView.cellForRow(at: indexPath)?.accessoryType = .none
//        return indexPath
        if indexPath == IndexPath(row: 0, section: 0){
            let index = IndexPath(row: 1, section: 0)
            tableView.cellForRow(at: index)?.accessoryType = .none
            checkedDisplay[index.row] = false
        }else if indexPath == IndexPath(row: 1, section: 1){
            let index = IndexPath(row: 0, section: 1)
            tableView.cellForRow(at: index)?.accessoryType = .none
            checkedInfo[index.row] = false
        }else if indexPath == IndexPath(row: 1, section: 0){
            let index = IndexPath(row: 0, section: 0)
            tableView.cellForRow(at: index)?.accessoryType = .none
            checkedDisplay[index.row] = false
        }else if indexPath == IndexPath(row: 0, section: 1){
            let index = IndexPath(row: 1, section: 1)
            tableView.cellForRow(at: index)?.accessoryType = .none
            checkedInfo[index.row] = false
        }
        return indexPath
    }
}
