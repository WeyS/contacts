

import UIKit
import RealmSwift

@available(iOS 13.0, *)
class SecondNewContactViewController: UIViewController {

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var contactImage: UIImageView!
    
    var realm = try! Realm()

    var picker = UIImagePickerController()
    var contact : [CreateCell] = []
    var licence: [CreateCell] = []
    var notes: [CreateCell] = []
    var contactValue : [Contact] = []
    var completionNewContact : (([Contact]) -> ())?


    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        contact = createArray()
        licence = createDriveLicence()
        notes = createNotes()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @IBAction func textFieldInCell(_ sender: UITextField) {
        if sender.text?.isEmpty == false{
            saveButton.isEnabled = true
        }else if sender.text?.isEmpty == true{
            saveButton.isEnabled = false
        }
    }
}

// MARK: - TableView
@available(iOS 13.0, *)
extension SecondNewContactViewController : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return contact.count
        case 1:
            return licence.count
        case 2:
            return notes.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return ""
        case 1:
            return " "
        case 2:
            return " "
        default:
            return " "
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        return self.tableView.backgroundColor = #colorLiteral(red: 0.9215685725, green: 0.9215685725, blue: 0.9215685725, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "1") as! RegViewCell
        
        switch indexPath.section{
        case 0:
            let contacts = contact[indexPath.row]
            cell.setContact(contact: contacts)
        case 1:
            let licence = licence[indexPath.row]
            cell.setDriver(licence: licence)
        case 2:
            let notes = notes[indexPath.row]
            cell.setNotes(notesCell: notes)
        default:
            break
        }

        return cell
    }
}

// MARK: - Button Navigation Tab
@available(iOS 13.0, *)
extension SecondNewContactViewController{
    func createArray() -> [CreateCell]{
        var tempContact: [CreateCell] = []

        let contact1 = CreateCell(title: "First Name".localizable(), placeHolder: "First Name".localizable(), validate: "Incorect name".localizable())
        let contact2 = CreateCell(title: "Last Name".localizable(), placeHolder: "Last Name".localizable(), validate: "Incorect last name(Enter 0-20 character)".localizable())
        let contact3 = CreateCell(title: "Phone Number".localizable(), placeHolder: "Phone Number".localizable(), validate: "Please use phone number format \"380972524401\"".localizable())
        let contact4 = CreateCell(title: "Email".localizable(), placeHolder: "Email".localizable(), validate: "Enter email in format: name@exaple.com".localizable())
        let contact5 = CreateCell(title: "Birthday".localizable(), placeHolder: "Birthday".localizable())
        let contact6 = CreateCell(title: "Height, cm".localizable(), placeHolder: "Height".localizable())

        tempContact.append(contact1)
        tempContact.append(contact2)
        tempContact.append(contact3)
        tempContact.append(contact4)
        tempContact.append(contact5)
        tempContact.append(contact6)

        return tempContact
    }
    func createDriveLicence() -> [CreateCell]{
        var tempLicence: [CreateCell] = []
        let licence1 = CreateCell(title: "Driving licence".localizable(), placeHolder: "")
        let licence2 = CreateCell(title: "Driving licence".localizable(), placeHolder: "Driver's licence number".localizable())
        tempLicence.append(licence1)
        tempLicence.append(licence2)
        return tempLicence
    }
    
    func createNotes() -> [CreateCell]{
        var tempNotes: [CreateCell] = []
        let notes = CreateCell(title: "Notes".localizable(), placeHolder: "Notes")
        tempNotes.append(notes)
        return tempNotes
    }

    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }


    @IBAction func saveButton(_ sender: Any) {
        let indexPathFirstName = IndexPath(row: 0, section: 0)
        let firstNameCell = tableView.cellForRow(at: indexPathFirstName) as! RegViewCell

        let cont1 = firstNameCell.textField.text!

        let indexPathLastName = IndexPath(row: 1, section: 0)
        let lastNameCell = tableView.cellForRow(at: indexPathLastName) as! RegViewCell

        let cont2 = lastNameCell.textField.text

        let indexPathPhone = IndexPath(row: 2, section: 0)
        let phoneNameCell = tableView.cellForRow(at: indexPathPhone) as! RegViewCell

        let cont3 = phoneNameCell.textField.text

        let indexPathEmail = IndexPath(row: 3, section: 0)
        let emailNameCell = tableView.cellForRow(at: indexPathEmail) as! RegViewCell

        let cont4 = emailNameCell.textField.text
        
        let indexPathDate = IndexPath(row: 4, section: 0)
        let dateNameCell = tableView.cellForRow(at: indexPathDate) as! RegViewCell

        let cont5 = dateNameCell.textField.text
        
        let indexPathHeight = IndexPath(row: 5, section: 0)
        let heightNameCell = tableView.cellForRow(at: indexPathHeight) as! RegViewCell

        let cont6 = heightNameCell.textField.text
        
        let indexPathDriverSwitch = IndexPath(row: 1, section: 1)
        let swicthDriverCell = tableView.cellForRow(at: indexPathDriverSwitch) as! RegViewCell

        let cont7 = swicthDriverCell.textField.text
        
        let indexPathNotes = IndexPath(row: 0, section: 2)
        let notesCell = tableView.cellForRow(at: indexPathNotes) as! RegViewCell

        let cont8 = notesCell.notes.text
        
        DispatchQueue.global().async { [self] in
            DispatchQueue.main.async {
                if firstNameCell.textField.text!.isEmpty == false && isValidNames(name: firstNameCell.textField.text!) != true {
                    firstNameCell.validate.isHidden = false
                }else if isValidNames(name: firstNameCell.textField.text!) == true{
                    firstNameCell.validate.isHidden = true
                }else if firstNameCell.textField.text!.isEmpty == true{
                    firstNameCell.validate.isHidden = true
                }

            }
            DispatchQueue.main.async {
                if lastNameCell.textField.text!.isEmpty == false && isValidNames(name: lastNameCell.textField.text!) != true{
                    lastNameCell.validate.isHidden = false
                }else if isValidNames(name: lastNameCell.textField.text!) == true{
                    lastNameCell.validate.isHidden = true
                }else if lastNameCell.textField.text!.isEmpty == true{
                    lastNameCell.validate.isHidden = true
                }
            }
            DispatchQueue.main.async {
                if phoneNameCell.textField.text!.isEmpty == false && isValidPhones(phone: phoneNameCell.textField.text!) != true {
                    phoneNameCell.validate.isHidden = false
                }else if isValidPhones(phone: phoneNameCell.textField.text!) == true{
                    phoneNameCell.validate.isHidden = true
                }else if phoneNameCell.textField.text!.isEmpty == true {
                    phoneNameCell.validate.isHidden = true
                }
            }
            DispatchQueue.main.async {
                if emailNameCell.textField.text?.isEmpty == false && isValidEmail(email: emailNameCell.textField.text!) != true{
                    emailNameCell.validate.isHidden = false
                }else if isValidEmail(email: emailNameCell.textField.text!) == true{
                    emailNameCell.validate.isHidden = true
                }else if emailNameCell.textField.text?.isEmpty == true{
                    emailNameCell.validate.isHidden = true
                }
            }
            DispatchQueue.main.async {
                if firstNameCell.validate.isHidden == true && lastNameCell.validate.isHidden == true && phoneNameCell.validate.isHidden == true && emailNameCell.validate.isHidden == true{
                    let data = (contactImage.image?.jpegData(compressionQuality: 0.9))!
                    let contact = Contact(firstName: cont1 , lastName: cont2 ?? "", phone: cont3 ?? "", email: cont4 ?? "", photo: data, date: cont5 ?? "", height: cont6 ?? "", driveNumber: cont7 ?? "", notes: cont8 ?? "")
                    contactValue.append(contact)
        //            completionNewContact?(contactValue)
                    try! realm.write{
                        realm.add(contactValue)
                    }
                    dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}

func areEqualImages(img1: UIImage, img2: UIImage) -> Bool {

    guard let data1 = img1.pngData() else { return false }
    guard let data2 = img2.pngData() else { return false }

    return data1 == data2
}

// MARK: - Botton add Photo
@available(iOS 13.0, *)
extension SecondNewContactViewController : UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    @IBAction func chooseProfilePicBtnClicked(sender: AnyObject) {
        if areEqualImages(img1: contactImage.image!, img2: UIImage(named: "Vector-1")!){
            let alert:UIAlertController=UIAlertController(title: "Choose variant", message: nil, preferredStyle: .actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: .default)
                {
                    UIAlertAction in
                    self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallary", style: .default)
                {
                    UIAlertAction in
                    self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                {
                    UIAlertAction in
                }

            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert:UIAlertController=UIAlertController(title: "What do you want to do", message: nil, preferredStyle: .actionSheet)
            let cameraAction = UIAlertAction(title: "Change photo", style: .default)
            {_ in
                let alert:UIAlertController=UIAlertController(title: "Choose variant", message: nil, preferredStyle: .actionSheet)
                let cameraAction = UIAlertAction(title: "Camera", style: .default)
                    {
                        UIAlertAction in
                        self.openCamera()
                }
                let gallaryAction = UIAlertAction(title: "Gallery", style: .default)
                    {
                        UIAlertAction in
                        self.openGallary()
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                    {
                        UIAlertAction in
                    }

                self.picker.delegate = self
                alert.addAction(cameraAction)
                alert.addAction(gallaryAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            let gallaryAction = UIAlertAction(title: "Remove photo", style: .default)
            {_ in
                self.contactImage.image = UIImage(named: "Vector-1")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                {
                    UIAlertAction in
                }

            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.sourceType = UIImagePickerController.SourceType.camera
            self.present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
        }
    }
    func openGallary(){
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    //MARK:UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            self.contactImage.image = pickedImage
            contactImage.layer.cornerRadius = contactImage.frame.size.width / 2
            contactImage.clipsToBounds = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        print("picker cancel.")
    }
}

@available(iOS 13.0, *)
extension SecondNewContactViewController{
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        tableView.contentInset = .zero
    }
}
